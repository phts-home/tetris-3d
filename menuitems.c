/*
******************************************************************************
*
* File                            menuitems.c
*
* Description                     ���� ���������� ������ menuitems.h
*
* Creation date                   03.10.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "menuitems.h"


/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
int   optionsItems(int);          // ����������� ����� ��������


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// �������� ������� ������� ��������
//
int showHighscores(void)
{
	MSG msg;
	int canExit = 0;
	char st[256], tmpSt[256];
	readRecords();

	while(!canExit){
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
			if(msg.message != WM_QUIT){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			SwapBuffers(hDC);
			if((keys[27])||(keys[13])){
				keys[13] = false;
				keys[27] = false;
				canExit = 1;
			}
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();
			glTranslatef(-10.0, 7.0, -20.0);
			glPrint(0.0, 0.0, 1.0, 0.0, 0.0, "HI-SCORES");
			glPrint(1.0, -1.5, 1.0, 1.0, 0.0, "NAME");
			glPrint(6.0, -1.5, 1.0, 1.0, 0.0, "SCORE");
			glPrint(11.0, -1.5, 1.0, 1.0, 0.0, "LINES");
			glPrint(16.0, -1.5, 1.0, 1.0, 0.0, "LEVEL");
			for(int i=0; i<10; i++){
				glPrint(1.0, -i-2.5, 0.5, 0.5, 0.5, recordTable[i].name);
				strcpy(st,_itoa(recordTable[i].score,tmpSt,10));
				glPrint(6.0, -i-2.5, 0.5, 0.5, 0.5, st);
				strcpy(st,_itoa(recordTable[i].lines,tmpSt,10));
				glPrint(11.0, -i-2.5, 0.5, 0.5, 0.5, st);
				strcpy(st,_itoa(recordTable[i].level,tmpSt,10));
				glPrint(16.0, -i-2.5, 0.5, 0.5, 0.5, st);
			}
		}
	}
	return 0;
}

//
// �������� ������� ���������� ���������
//
int showOptions(void)
{
	MSG msg;
	int item = 10;
	int canExit = 0;

	while(!canExit){
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
			if(msg.message != WM_QUIT){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			glClearColor(0.0, 0.0, 0.0, 0.5);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();
			glTranslatef(-10.0, 7.0, -20.0);

			glPrint(0.0, 0.0, 1.0, 0.0, 0.0, "OPTIONS");
			glPrint(1.0, -1.5, 1.0, 1.0, 0.0, "graphics");
			glPrint(1.0, -4.5, 1.0, 1.0, 0.0, "sound");
			glPrint(1.0, -7.0, 1.0, 1.0, 0.0, "interface");
			glPrint(1.0, -10.0, 1.0, 1.0, 0.0, "controls");
			optionsItems(item);
			SwapBuffers(hDC);
			if(keys[13]){
				keys[13] = false;
				switch(item){
					case 0:
						parameters.grfWndSize %= 2;
						parameters.grfWndSize += 1;
						destroyWindow();
						if(parameters.grfWndSize == 1) createWindow(1024,768); else createWindow(800,600);
						initTextures();
						applyTexture(0);
						initGL();
						destroyFont();
						initFont();
						break;
					case 1:
						parameters.grfBackgrTexture = !parameters.grfBackgrTexture;
						break;
					case 2:
						parameters.grfBottomTexture = !parameters.grfBottomTexture;
						break;
					case 3:
						parameters.sndSoundInGame = !parameters.sndSoundInGame;
						break;
					case 4:
						parameters.sndSoundInMenu = !parameters.sndSoundInMenu;
						break;
					case 5:
						parameters.intrfMenuAnimation = !parameters.intrfMenuAnimation;
						break;
					case 6:
						parameters.intrfLineAnimation = !parameters.intrfLineAnimation;
						break;
					case 7:
						parameters.intrfFigAnimation = !parameters.intrfFigAnimation;
						break;
					case 8:
						defineKeys();
						break;
					case 9:
						initKeys();
						break;
					case 10:
						canExit = 1;
						break;
				}
				saveOptions();
			}
			if(keys[27]){
				keys[27] = false;
				canExit = 1;
			}
			if(keys[VK_UP]){
				keys[VK_UP] = false;
				item--;
				if(item < 0) item = 10;
			}
			if(keys[VK_DOWN]){
				keys[VK_DOWN] = false;
				item++;
				item %= 11;
			}
		}
	}
	return 0;
}

//
// �������� ������� ������
//
int showHelp(void)
{
	MSG msg;
	int canExit = 0;

	while(!canExit){
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
			if(msg.message != WM_QUIT){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			SwapBuffers(hDC);
			if((keys[27])||(keys[13])){
				keys[13] = false;
				keys[27] = false;
				canExit = 1;
			}
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();
			glTranslatef(-10.0, 7.0, -20.0);
			glPrint(0.0, 0.0, 1.0, 0.0, 0.0, "INFORMATION");
			glPrint(1.0, -1.5, 1.0, 1.0, 0.0, "game rules");
			glPrint(1.0, -2.5, 0.5, 0.5, 0.5, "the rule is only one: clear lines and gather points");
			glPrint(1.0, -3.0, 0.5, 0.5, 0.5, "each 100 pt. the speed will be increased");
			glPrint(1.0, -3.5, 0.5, 0.5, 0.5, "each 1000 pt. the level will be increased");
			glPrint(1.0, -4.0, 0.5, 0.5, 0.5, "each new level adds a new difficult figure");

			glPrint(1.0, -5.0, 1.0, 1.0, 0.0, "standart controls");
			glPrint(1.0, -6.0, 0.5, 0.5, 0.5, "arrows left/right/down    move figure");
			glPrint(1.0, -6.5, 0.5, 0.5, 0.5, "arrow up                  rotate figure");
			glPrint(1.0, -7.0, 0.5, 0.5, 0.5, "space                     fast move down");
			glPrint(1.0, -7.5, 0.5, 0.5, 0.5, "P                         pause");
			glPrint(1.0, -8.0, 0.5, 0.5, 0.5, "F, H, T, G                move camera");
			glPrint(1.0, -8.5, 0.5, 0.5, 0.5, "A, D, W, S                rotate camera");
			glPrint(1.0, -9.0, 0.5, 0.5, 0.5, "Z, X                      zoom in/out");
			glPrint(1.0, -9.5, 0.5, 0.5, 0.5, "C                         switch cameras");
			glPrint(1.0, -10.0, 0.5, 0.5, 0.5, "esc                       game menu");
		}
	}
	return 0;
}

//
// �������� ������� "� ���������"
//
int showAbout(void)
{
	MSG msg;
	int canExit = 0;

	while(!canExit){
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
			if(msg.message != WM_QUIT){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			SwapBuffers(hDC);
			if((keys[27])||(keys[13])){
				keys[13] = false;
				keys[27] = false;
				canExit = 1;
			}
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();
			glTranslatef(-10.0, 7.0, -20.0);
			glPrint(0.0, 0.0, 1.0, 0.0, 0.0, "ABOUT");
			glPrint(1.0, -1.5, 1.0, 1.0, 0.0, "tetris 3D");
			glPrint(1.0, -2.5, 0.5, 0.5, 0.5, "version              1.0 (build 90) [final]");
			glPrint(1.0, -3.0, 0.5, 0.5, 0.5, "author               phil tsarik");
			glPrint(1.0, -3.5, 0.5, 0.5, 0.5, "e-mail               philip-s@yandex.ru");
			glPrint(1.0, -4.0, 0.5, 0.5, 0.5, "web                  http://philip-s.narod.ru");
		}
	}
	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ����������� ����� ��������
//
int optionsItems(int item)
{
	char strparam[8][33] = {
		"window size          ",
		"backgr texture       ",
		"bottom texture       ",
		"sound in game        ",
		"sound in menu        ",
		"menu animation       ",
		"line animation       ",
		"next fig. animation  ",
		};
	if(parameters.grfWndSize == 1) strcat(strparam[0], "[1024x768]"); else strcat(strparam[0], "[800x600]");
	if(parameters.grfBackgrTexture) strcat(strparam[1], "[enable]"); else strcat(strparam[1], "[disable]");
	if(parameters.grfBottomTexture) strcat(strparam[2], "[enable]"); else strcat(strparam[2], "[disable]");
	if(parameters.sndSoundInGame) strcat(strparam[3], "[enable]"); else strcat(strparam[3], "[disable]");
	if(parameters.sndSoundInMenu) strcat(strparam[4], "[enable]"); else strcat(strparam[4], "[disable]");
	if(parameters.intrfMenuAnimation) strcat(strparam[5], "[enable]"); else strcat(strparam[5], "[disable]");
	if(parameters.intrfLineAnimation) strcat(strparam[6], "[enable]"); else strcat(strparam[6], "[disable]");
	if(parameters.intrfFigAnimation) strcat(strparam[7], "[enable]"); else strcat(strparam[7], "[disable]");
	if(item != 0){
		glPrint(1.0, -2.5, 0.5, 0.5, 0.5, strparam[0]);
	} else {
		glPrint(1.0, -2.5, 1.0, 1.0, 1.0, strparam[0]);
	}
	if(item != 1){
		glPrint(1.0, -3.0, 0.5, 0.5, 0.5, strparam[1]);
	} else {
		glPrint(1.0, -3.0, 1.0, 1.0, 1.0, strparam[1]);
	}
	if(item != 2){
		glPrint(1.0, -3.5, 0.5, 0.5, 0.5, strparam[2]);
	} else {
		glPrint(1.0, -3.5, 1.0, 1.0, 1.0, strparam[2]);
	}
	if(item != 3){
		glPrint(1.0, -5.5, 0.5, 0.5, 0.5, strparam[3]);
	} else {
		glPrint(1.0, -5.5, 1.0, 1.0, 1.0, strparam[3]);
	}
	if(item != 4){
		glPrint(1.0, -6.0, 0.5, 0.5, 0.5, strparam[4]);
	} else {
		glPrint(1.0, -6.0, 1.0, 1.0, 1.0, strparam[4]);
	}
	if(item != 5){
		glPrint(1.0, -8.0, 0.5, 0.5, 0.5, strparam[5]);
	} else {
		glPrint(1.0, -8.0, 1.0, 1.0, 1.0, strparam[5]);
	}
	if(item != 6){
		glPrint(1.0, -8.5, 0.5, 0.5, 0.5, strparam[6]);
	} else {
		glPrint(1.0, -8.5, 1.0, 1.0, 1.0, strparam[6]);
	}
	if(item != 7){
		glPrint(1.0, -9.0, 0.5, 0.5, 0.5, strparam[7]);
	} else {
		glPrint(1.0, -9.0, 1.0, 1.0, 1.0, strparam[7]);
	}
	if(item != 8){
		glPrint(1.0, -11.0, 0.5, 0.5, 0.5, "redefine");
	} else {
		glPrint(1.0, -11.0, 1.0, 1.0, 1.0, "redefine");
	}
	if(item != 9){
		glPrint(1.0, -11.5, 0.5, 0.5, 0.5, "reset to default values");
	} else {
		glPrint(1.0, -11.5, 1.0, 1.0, 1.0, "reset to default values");
	}
	if(item != 10){
		glPrint(1.0, -13.5, 0.5, 0.5, 0.5, "back");
	} else {
		glPrint(1.0, -13.5, 1.0, 1.0, 1.0, "back");
	}
	return 0;
}
