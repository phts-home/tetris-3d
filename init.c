/*
******************************************************************************
*
* File                            init.c
*
* Description                     ���� ���������� ������ init.h
*
* Creation date                   21.09.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "init.h"


/*
******************************************************************************
*                                 LOCAL CONSTANTS
******************************************************************************
*/
static const int patterns[48][16] = {
	{1,0,0,0, 1,0,0,0, 1,1,0,0, 0,0,0,0},
	{0,0,0,0, 1,1,1,0, 1,0,0,0, 0,0,0,0},
	{1,1,0,0, 0,1,0,0, 0,1,0,0, 0,0,0,0},
	{0,0,0,0, 0,0,1,0, 1,1,1,0, 0,0,0,0},

	{0,2,0,0, 0,2,0,0, 2,2,0,0, 0,0,0,0},
	{0,0,0,0, 2,0,0,0, 2,2,2,0, 0,0,0,0},
	{2,2,0,0, 2,0,0,0, 2,0,0,0, 0,0,0,0},
	{0,0,0,0, 2,2,2,0, 0,0,2,0, 0,0,0,0},

	{0,0,0,0, 0,3,0,0, 3,3,3,0, 0,0,0,0},
	{0,3,0,0, 0,3,3,0, 0,3,0,0, 0,0,0,0},
	{0,0,0,0, 3,3,3,0, 0,3,0,0, 0,0,0,0},
	{0,3,0,0, 3,3,0,0, 0,3,0,0, 0,0,0,0},

	{0,0,0,0, 4,4,0,0, 0,4,4,0, 0,0,0,0},
	{0,4,0,0, 4,4,0,0, 4,0,0,0, 0,0,0,0},
	{0,0,0,0, 4,4,0,0, 0,4,4,0, 0,0,0,0},
	{0,4,0,0, 4,4,0,0, 4,0,0,0, 0,0,0,0},

	{0,0,0,0, 0,5,5,0, 5,5,0,0, 0,0,0,0},
	{5,0,0,0, 5,5,0,0, 0,5,0,0, 0,0,0,0},
	{0,0,0,0, 0,5,5,0, 5,5,0,0, 0,0,0,0},
	{5,0,0,0, 5,5,0,0, 0,5,0,0, 0,0,0,0},

	{0,6,0,0, 0,6,0,0, 0,6,0,0, 0,6,0,0},
	{0,0,0,0, 6,6,6,6, 0,0,0,0, 0,0,0,0},
	{0,6,0,0, 0,6,0,0, 0,6,0,0, 0,6,0,0},
	{0,0,0,0, 6,6,6,6, 0,0,0,0, 0,0,0,0},

	{7,7,0,0, 7,7,0,0, 0,0,0,0, 0,0,0,0},
	{7,7,0,0, 7,7,0,0, 0,0,0,0, 0,0,0,0},
	{7,7,0,0, 7,7,0,0, 0,0,0,0, 0,0,0,0},
	{7,7,0,0, 7,7,0,0, 0,0,0,0, 0,0,0,0},

	{0,0,0,0, 8,0,8,0, 8,8,8,0, 0,0,0,0},
	{8,8,0,0, 8,0,0,0, 8,8,0,0, 0,0,0,0},
	{0,0,0,0, 8,8,8,0, 8,0,8,0, 0,0,0,0},
	{8,8,0,0, 0,8,0,0, 8,8,0,0, 0,0,0,0},

	{0,9,0,0, 0,9,0,0, 9,9,9,0, 0,0,0,0},
	{9,0,0,0, 9,9,9,0, 9,0,0,0, 0,0,0,0},
	{9,9,9,0, 0,9,0,0, 0,9,0,0, 0,0,0,0},
	{0,0,9,0, 9,9,9,0, 0,0,9,0, 0,0,0,0},

	{0,10,0,0, 10,10,10,0, 0,10,0,0, 0,0,0,0},
	{0,10,0,0, 10,10,10,0, 0,10,0,0, 0,0,0,0},
	{0,10,0,0, 10,10,10,0, 0,10,0,0, 0,0,0,0},
	{0,10,0,0, 10,10,10,0, 0,10,0,0, 0,0,0,0},

	{11,11,0,0, 0,11,0,0, 0,11,11,0, 0,0,0,0},
	{0,0,11,0, 11,11,11,0, 11,0,0,0, 0,0,0,0},
	{11,11,0,0, 0,11,0,0, 0,11,11,0, 0,0,0,0},
	{0,0,11,0, 11,11,11,0, 11,0,0,0, 0,0,0,0},

	{0,12,12,0, 0,12,0,0, 12,12,0,0, 0,0,0,0},
	{12,0,0,0, 12,12,12,0, 0,0,12,0, 0,0,0,0},
	{0,12,12,0, 0,12,0,0, 12,12,0,0, 0,0,0,0},
	{12,0,0,0, 12,12,12,0, 0,0,12,0, 0,0,0,0}
};


/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
AUX_RGBImageRec *loadTexture(char []);// �������� �������� �� �����


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// �������� ������������� �������
//
int **createAr(int width, int height)
{
	int **ar;
	ar = (int **)calloc(width, sizeof(int *));
	for(int i=0; i<width; i++)
		ar[i] = (int *)calloc(height, sizeof(int));
	return ar;
}

//
// ������������ ������, ���������� ��� ������
//
int destroyAr(int **ar)
{
	for(int i=0; i<10; i++) free(ar[i]);
	free(ar);
	return 0;
}

//
// �������� ������� �����
//
int createFigures(int **figures[], int amount)
{
	for(int n=0; n<amount; n++){
		figures[n] = createAr(4,4);
		for(int i=0; i<4; i++)
			for(int j=0; j<4; j++){
				figures[n][i][j] = patterns[n][i+j*4];
			}
	}
	return 0;
}

//
// ������������� �������� ����
//
int initField(void)
{
	field.width = 12;
	field.height = 22;
	field.ar = createAr(field.width,field.height);
	field.x = 1;
	field.y = 1;
	return 0;
}

//
// ������������� ����������, ���������� ������
//
int initFig(void)
{
	fig.width = 4;
	fig.height = 4;
	fig.ar = createAr(fig.width,fig.height);
	fig.x = 1;
	fig.y = 1;
	return 0;
}

//
// ������������� �������
//
int initGL(void)
{
	glEnable(GL_TEXTURE_2D);

	glShadeModel(GL_SMOOTH);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	return 0;
}

//
// ��������� ������� �����
//
int resizeGL(int width, int height)
{
	if (height == 0) height = 1;

	glViewport(0,0,width,height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0,(double)width/(double)height,0.1,100.0);

	glMatrixMode(GL_MODELVIEW);
	return 0;
}

//
// ������������� ������
//
int initFont(void)
{
	HFONT font;
	base = glGenLists(96);
	font = CreateFont(-20,0,0,0,FW_BOLD,false,false,false,ANSI_CHARSET,OUT_TT_PRECIS,
		CLIP_DEFAULT_PRECIS,ANTIALIASED_QUALITY,FF_DONTCARE|DEFAULT_PITCH,"Courier New");
	SelectObject(hDC, font);
	wglUseFontBitmaps(hDC, 32, 96, base);
	return 0;
}

//
// �������� ������ �� ������
//
int destroyFont(void)
{
   glDeleteLists(base, 96);
	return 0;
}

//
// �������� ������� �� ������ � ������
//
int initTextures(void)
{
	textures[0] = loadTexture("textures\\mCube.bmp");
	textures[1] = loadTexture("textures\\m3D.bmp");
	textures[2] = loadTexture("textures\\mCharA.bmp");
	textures[3] = loadTexture("textures\\mCharB.bmp");
	textures[4] = loadTexture("textures\\mCharC.bmp");
	textures[5] = loadTexture("textures\\mCharD.bmp");
	textures[6] = loadTexture("textures\\mCharE.bmp");
	textures[7] = loadTexture("textures\\mCharF.bmp");
	textures[8] = loadTexture("textures\\mCharG.bmp");
	textures[9] = loadTexture("textures\\mCharH.bmp");
	textures[10] = loadTexture("textures\\mCharI.bmp");
	textures[11] = loadTexture("textures\\mCharJ.bmp");
	textures[12] = loadTexture("textures\\mCharK.bmp");
	textures[13] = loadTexture("textures\\mCharL.bmp");
	textures[14] = loadTexture("textures\\mCharM.bmp");
	textures[15] = loadTexture("textures\\mCharN.bmp");
	textures[16] = loadTexture("textures\\mCharO.bmp");
	textures[17] = loadTexture("textures\\mCharP.bmp");
	textures[18] = loadTexture("textures\\mCharQ.bmp");
	textures[19] = loadTexture("textures\\mCharR.bmp");
	textures[20] = loadTexture("textures\\mCharS.bmp");
	textures[21] = loadTexture("textures\\mCharT.bmp");
	textures[22] = loadTexture("textures\\mCharU.bmp");
	textures[23] = loadTexture("textures\\mCharV.bmp");
	textures[24] = loadTexture("textures\\mCharW.bmp");
	textures[25] = loadTexture("textures\\mCharX.bmp");
	textures[26] = loadTexture("textures\\mCharY.bmp");
	textures[27] = loadTexture("textures\\mCharZ.bmp");
	textures[28] = loadTexture("textures\\mCursor.bmp");
	textures[29] = loadTexture("textures\\tGround.bmp");
	textures[30] = loadTexture("textures\\tBackgr.bmp");
	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// �������� �������� �� �����
//
AUX_RGBImageRec *loadTexture(char path[])
{
	FILE *file = fopen(path,"r");
	if( file == NULL ){
		char str[257] = "Error. Unable to load texture from file:\n";
		strcat(str,path);
		MessageBox(NULL,str,"Tetris 3D",MB_OK|MB_ICONERROR);
		exit(0);
	}
	fclose(file);
	return auxDIBImageLoad(path);
}
