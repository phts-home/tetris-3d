/*
******************************************************************************
*
* File                            options.h
*
* Description                     ���������� �������� ���������
*
* Creation date                   04.10.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 MODULE BEGIN
******************************************************************************
*/
#ifndef OPTIONS_H
#define OPTIONS_H


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
// �����������
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <stdbool.h>
// ��� ������ � OpenGL
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>
// ����������������
#include "interface.h"


/*
******************************************************************************
*                                 GLOBAL CONSTANTS
******************************************************************************
*/
#define  KEY_FIG_LEFT      0
#define  KEY_FIG_RIGHT     1
#define  KEY_FIG_DOWN      2
#define  KEY_FIG_ROTATE    3
#define  KEY_FIG_FASTMOVE  4
#define  KEY_GAME_PAUSE    5
#define  KEY_CAM_LEFT      6
#define  KEY_CAM_RIGHT     7
#define  KEY_CAM_DOWN      8
#define  KEY_CAM_UP        9
#define  KEY_CAM_RLEFT    10
#define  KEY_CAM_RRIGHT   11
#define  KEY_CAM_RDOWN    12
#define  KEY_CAM_RUP      13
#define  KEY_CAM_ZOOMIN   14
#define  KEY_CAM_ZOOMOUT  15
#define  KEY_CAM_SWITCH   16


/*
******************************************************************************
*                                 GLOBAL DATA TYPES
******************************************************************************
*/
typedef struct {
   int   grfWndSize;              // "window size"
   bool  grfBackgrTexture;        // "backgr texture"
   bool  grfBottomTexture;        // "bottom texture"
   bool  sndSoundInGame;          // "sound in game"
   bool  sndSoundInMenu;          // "sound in menu"
   bool  intrfMenuAnimation;      // "menu animation"
   bool  intrfLineAnimation;      // "line animation"
   bool  intrfFigAnimation;       // "next fig. animation"
   int   ctrlKeys[17];            // ��������� ������ ����������
} TParameters;


/*
******************************************************************************
*                                 GLOBAL VARIABLES
******************************************************************************
*/
TParameters parameters;           // ��������� ���������
FILE *fOptions;                  // ��������� �� ���� � ����������� ����


/*
******************************************************************************
*                                 GLOBAL PROTOTYPES
******************************************************************************
*/
int   initKeys(void);             // ����� �������� ���� ������ �� �����������
int   defineKeys(void);           // ��������������� ������ ����������
int   readOptions(void);          // ������ ���������� �� �����
int   saveOptions(void);          // ���������� ���������� � ����


/*
******************************************************************************
*                                 MODULE END
******************************************************************************
*/
#endif
