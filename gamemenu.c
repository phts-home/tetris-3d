/*
******************************************************************************
*
* File                            gamemenu.c
*
* Description                     ���� ���������� ������ gamemenu.h
*
* Creation date                   03.10.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "gamemenu.h"


/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
int   gamemenuItems(int);         // ��������� ����������� ������


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ������� ����
//
//    ������������ ��������:
//       0                        "return", esc
//       1                        "restart"
//       3                        "exit to mainmenu"
//       4                        "exit to windows"
//
int showGamemenu(void)
{
	MSG msg;
	int item = 0;
	int canExit = 0;

	glClearColor(0.0, 0.0, 0.0, 0.5);
	while(!canExit){
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
			if(msg.message != WM_QUIT){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();
			glTranslatef(-10.0, 7.0, -20.0);
			glPrint(0.0, 0.0, 1.0, 0.0, 0.0, "GAME MENU");
			gamemenuItems(item);
			SwapBuffers(hDC);
			if(keys[13]){
				keys[13] = false;
				if(item == 2){
					showOptions();
				} else {
					return item;
				}
			}
			if(keys[27]){
				keys[27] = false;
				canExit = 1;
			}
			if(keys[VK_UP]){
				keys[VK_UP] = false;
				item--;
				if(item < 0) item = 4;
			}
			if(keys[VK_DOWN]){
				keys[VK_DOWN] = false;
				item++;
				item %= 5;
			}
		}
	}
	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ��������� ����������� ������
//
int gamemenuItems(int item)
{
	if(item != 0){
		glPrint(1.0, -1.5, 0.5, 0.5, 0.5, "return to game");
	} else {
		glPrint(1.0, -1.5, 1.0, 1.0, 1.0, "return to game");
	}
	if(item != 1){
		glPrint(1.0, -2.5, 0.5, 0.5, 0.5, "restart");
	} else {
		glPrint(1.0, -2.5, 1.0, 1.0, 1.0, "restart");
	}
	if(item != 2){
		glPrint(1.0, -3.5, 0.5, 0.5, 0.5, "options");
	} else {
		glPrint(1.0, -3.5, 1.0, 1.0, 1.0, "options");
	}
	if(item != 3){
		glPrint(1.0, -12.5, 0.5, 0.5, 0.5, "exit to mainmenu");
	} else {
		glPrint(1.0, -12.5, 1.0, 1.0, 1.0, "exit to mainmenu");
	}
	if(item != 4){
		glPrint(1.0, -13.5, 0.5, 0.5, 0.5, "exit to windows");
	} else {
		glPrint(1.0, -13.5, 1.0, 1.0, 1.0, "exit to windows");
	}
	return 0;
}
