/*
******************************************************************************
*
* File                            options.c
*
* Description                     ���� ���������� ������ options.h
*
* Creation date                   04.10.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "options.h"


/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
int   drwActions(int);            // ����������� ������� ����������� �������
int   drwKeys(int);               // ����������� ������


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ����� �������� ���� ������ �� �����������
//
int initKeys(void)
{
	parameters.ctrlKeys[KEY_FIG_LEFT] = VK_LEFT;
	parameters.ctrlKeys[KEY_FIG_RIGHT] = VK_RIGHT;
	parameters.ctrlKeys[KEY_FIG_DOWN] = VK_DOWN;
	parameters.ctrlKeys[KEY_FIG_ROTATE] = VK_UP;
	parameters.ctrlKeys[KEY_FIG_FASTMOVE] = 32;
	parameters.ctrlKeys[KEY_GAME_PAUSE] = 'P';
	parameters.ctrlKeys[KEY_CAM_LEFT] = 'F';
	parameters.ctrlKeys[KEY_CAM_RIGHT] = 'H';
	parameters.ctrlKeys[KEY_CAM_DOWN] = 'G';
	parameters.ctrlKeys[KEY_CAM_UP] = 'T';
	parameters.ctrlKeys[KEY_CAM_RLEFT] = 'A';
	parameters.ctrlKeys[KEY_CAM_RRIGHT] = 'D';
	parameters.ctrlKeys[KEY_CAM_RDOWN] = 'S';
	parameters.ctrlKeys[KEY_CAM_RUP] = 'W';
	parameters.ctrlKeys[KEY_CAM_ZOOMIN] = 'Z';
	parameters.ctrlKeys[KEY_CAM_ZOOMOUT] = 'X';
	parameters.ctrlKeys[KEY_CAM_SWITCH] = 'C';
	return 0;
}

//
// ��������������� ������ ����������
//
int defineKeys(void)
{
	MSG msg;
	int canExit = 0;
	int amount = 0;
	
	while(!canExit){
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
			if(msg.message != WM_QUIT){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			drwActions(amount);
			drwKeys(amount);
			SwapBuffers(hDC);
			if(keys[27]){
				keys[27] = false;
				glClearColor(0.0, 0.0, 0.0, 0.5);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
				glLoadIdentity();
				glTranslatef(-10.0, 7.0, -20.0);
				return 1;
			}
			for(int i=0; i<256; i++){
				if(keys[i]){
					keys[i] = false;
					parameters.ctrlKeys[amount] = i;
					amount++;
					break;
				}
			}
			if(amount > 16){
				canExit = 1;
			}
		}
	}
	glClearColor(0.0, 0.0, 0.0, 0.5);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(-10.0, 7.0, -20.0);
	return 0;
}

//
// ������ ���������� �� �����
//
int readOptions(void)
{
	fOptions = fopen("options.dat", "rb");
	if(fOptions == NULL){
		parameters.grfWndSize = 2;
		parameters.grfBackgrTexture = true;
		parameters.grfBottomTexture = true;
		parameters.sndSoundInGame = true;
		parameters.sndSoundInMenu = true;
		parameters.intrfMenuAnimation = true;
		parameters.intrfLineAnimation = true;
		parameters.intrfFigAnimation = true;
		initKeys();
		saveOptions();
		return 0;
	}
	fread(&parameters, sizeof(TParameters), 1, fOptions);
	fclose(fOptions);
	return 0;
}

//
// ���������� ���������� � ����
//
int saveOptions(void)
{
	fOptions = fopen("options.dat", "wb");
	fwrite(&parameters, sizeof(TParameters), 1, fOptions);
	fclose(fOptions);
	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ����������� ������� ����������� �������
//
int drwActions(int amount)
{
	glClearColor(0.0, 0.0, 0.0, 0.5);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(-10.0, 7.0, -20.0);
	
	switch(amount){
		case 16: glPrint(0.0, -8.0, 1.0, 1.0, 1.0, "switch cameras");
		case 15: glPrint(0.0, -7.5, 1.0, 1.0, 1.0, "zoom out");
		case 14: glPrint(0.0, -7.0, 1.0, 1.0, 1.0, "zoom in");
		case 13: glPrint(0.0, -6.5, 1.0, 1.0, 1.0, "rotate camera up");
		case 12: glPrint(0.0, -6.0, 1.0, 1.0, 1.0, "rotate camera down");
		case 11: glPrint(0.0, -5.5, 1.0, 1.0, 1.0, "rotate camera right");
		case 10: glPrint(0.0, -5.0, 1.0, 1.0, 1.0, "rotate camera left");
		case 9: glPrint(0.0, -4.5, 1.0, 1.0, 1.0, "move camera up");
		case 8: glPrint(0.0, -4.0, 1.0, 1.0, 1.0, "move camera down");
		case 7: glPrint(0.0, -3.5, 1.0, 1.0, 1.0, "move camera right");
		case 6: glPrint(0.0, -3.0, 1.0, 1.0, 1.0, "move camera left");
		case 5: glPrint(0.0, -2.5, 1.0, 1.0, 1.0, "pause");
		case 4: glPrint(0.0, -2.0, 1.0, 1.0, 1.0, "fast move down");
		case 3: glPrint(0.0, -1.5, 1.0, 1.0, 1.0, "rotate figure");
		case 2: glPrint(0.0, -1.0, 1.0, 1.0, 1.0, "move figure down");
		case 1: glPrint(0.0, -0.5, 1.0, 1.0, 1.0, "move figure right");
		case 0: glPrint(0.0, 0.0, 1.0, 1.0, 1.0, "move figure left");
	}
	return 0;
}

//
// ����������� ������
//
int drwKeys(int amount)
{
	char k[17];
	for(int i=0; i<amount; i++){
		switch(parameters.ctrlKeys[i]){
			case 37: strcpy(k, "left"); break;
			case 39: strcpy(k, "right"); break;
			case 40: strcpy(k, "down"); break;
			case 38: strcpy(k, "up"); break;
			case 32: strcpy(k, "space"); break;
			case 13: strcpy(k, "enter"); break;
			case 16: strcpy(k, "shift"); break;
			case 17: strcpy(k, "ctrl"); break;
			case 18: strcpy(k, "alt"); break;
			case 9: strcpy(k, "tab"); break;
			case 8: strcpy(k, "backspace"); break;
			case 45: strcpy(k, "ins"); break;
			case 46: strcpy(k, "del"); break;
			case 33: strcpy(k, "page up"); break;
			case 34: strcpy(k, "page down"); break;
			case 35: strcpy(k, "end"); break;
			case 36: strcpy(k, "home"); break;
			case 96: strcpy(k, "num 0"); break;
			case 97: strcpy(k, "num 1"); break;
			case 98: strcpy(k, "num 2"); break;
			case 99: strcpy(k, "num 3"); break;
			case 100: strcpy(k, "num 4"); break;
			case 101: strcpy(k, "num 5"); break;
			case 102: strcpy(k, "num 6"); break;
			case 103: strcpy(k, "num 7"); break;
			case 104: strcpy(k, "num 8"); break;
			case 105: strcpy(k, "num 9"); break;
			case 106: strcpy(k, "num *"); break;
			case 107: strcpy(k, "num +"); break;
			case 109: strcpy(k, "num -"); break;
			case 110: strcpy(k, "num ."); break;
			case 111: strcpy(k, "num /"); break;
			case 112: strcpy(k, "F1"); break;
			case 113: strcpy(k, "F2"); break;
			case 114: strcpy(k, "F3"); break;
			case 115: strcpy(k, "F4"); break;
			case 116: strcpy(k, "F5"); break;
			case 117: strcpy(k, "F6"); break;
			case 118: strcpy(k, "F7"); break;
			case 119: strcpy(k, "F8"); break;
			case 120: strcpy(k, "F9"); break;
			case 121: strcpy(k, "F10"); break;
			case 122: strcpy(k, "F11"); break;
			case 123: strcpy(k, "F12"); break;
			case 186: strcpy(k, ";"); break;
			case 187: strcpy(k, "="); break;
			case 188: strcpy(k, ","); break;
			case 189: strcpy(k, "-"); break;
			case 190: strcpy(k, "."); break;
			case 191: strcpy(k, "/"); break;
			case 192: strcpy(k, "`"); break;
			case 219: strcpy(k, "["); break;
			case 220: strcpy(k, "\\"); break;
			case 221: strcpy(k, "]"); break;
			case 222: strcpy(k, "'"); break;
			default:
				k[0] = parameters.ctrlKeys[i];
				k[1] = 0;
		}
		glPrint(10.0, -i*0.5, 1.0, 1.0, 0.0, k);
	}
	return 0;
}
