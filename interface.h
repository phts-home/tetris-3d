/*
******************************************************************************
*
* File                            interface.h
*
* Description                     ���������� ���������� ����
*
* Creation date                   28.09.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 MODULE BEGIN
******************************************************************************
*/
#ifndef INTERFACE_H
#define INTERFACE_H


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
// �����������
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <sys\timeb.h>
// ��� ������ � OpenGL
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>
// ����������������
#include "window.h"
#include "game.h"
#include "camera.h"
#include "options.h"


/*
******************************************************************************
*                                 GLOBAL VARIABLES
******************************************************************************
*/
struct _timeb fitbeg, fitend;     // ���������� ��� ������������� ��������
int   base;                       // ���������� ��� ������ �� ��������
float fi;                         // ���� �������� ������ ��� �����������
AUX_RGBImageRec *textures[31];    // �������� ��� �������� ����������


/*
******************************************************************************
*                                 GLOBAL PROTOTYPES
******************************************************************************
*/
int   applyTexture(int);          // ���������� �������� � ����. �������

int   drwField(void);             // ��������� ����
int   drwFig(void);               // ��������� ������
int   drwBackgr(void);            // ������ ���
int   drwGround(void);            // ������ �����������
int   drwGame(void);              // ��������� ����
int   drwDelLine(int);            // ������ �������� �����

int   drwCube(float, float, float,// ������ ���
              int);
int   glPrint(float, float, float,// ������ ��������� �����
              float, float, const char *, ...);
int   drwWord(float, float, float,// ������ ������ ��������
              float, float, float, int, char *);

int   showNext(int **, float);    // ���������� ��������� ������
int   showPause(float);           // ����������� "PAUSE"
int   showGameover(float);        // ����������� "GAMEOVER"
int   showScore(int, int, int,    // ����������� �����
                int, float, float, float);


/*
******************************************************************************
*                                 MODULE END
******************************************************************************
*/
#endif
