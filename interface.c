/*
******************************************************************************
*
* File                            interface.c
*
* Description                     ���� ���������� ������ interface.h
*
* Creation date                   28.09.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "interface.h"


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ���������� �������� � ������������ �������
//
int applyTexture(int index)
{
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, textures[index]->sizeX, textures[index]->sizeY, 0,
		GL_RGB, GL_UNSIGNED_BYTE, textures[index]->data);
	return 0;
}

//
// ��������� ����
//
int drwField(void)
{
	for(int i=0; i<field.width; i++){
		for(int j=0; j<field.height; j++){
			if(field.ar[i][j] != 0){
				drwCube(i,j,-0.5,field.ar[i][j]);
			}
			if( (j == 0) || (j == field.height-1) || (i == 0) || (i == field.width-1) ){
				drwCube(i,j,-1.5,field.ar[i][j]);
				drwCube(i,j,0.5,field.ar[i][j]);
			}
		}
	}
	return 0;
}

//
// ��������� ������
//
int drwFig(void)
{
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
			if(fig.ar[i][j] != 0){
				drwCube(i+fig.x,j+fig.y,-0.5,fig.ar[i][j]);
			}
	return 0;
}

//
// ��������� ����
//
int drwBackgr(void)
{
	applyTexture(30);
	glBegin(GL_QUADS);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(0.5, 0.5, -1.4);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(11.5, 0.5, -1.4);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(11.5, -20.5, -1.4);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(0.5, -20.5, -1.4);
	glEnd();
	applyTexture(0);
	return 0;
}

//
// ��������� �����
//
int drwGroung(void)
{
	applyTexture(29);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-20.0, -21.0, -26.0);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(32.0, -21.0, -26.0);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(32.0, -21.0, 26.0);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(-20.0, -21.0, 26.0);
	glEnd();
	applyTexture(0);
	return 0;
}

//
// ��������� ����
//
int drwGame(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if( parameters.intrfFigAnimation ){
		_ftime(&fitend);
		if( ((fitend.time-fitbeg.time)*1000+fitend.millitm)-fitbeg.millitm >= 50){
			_ftime(&fitbeg);
			fi += 2.0;
		}
	} else {
		fi = 0.0;
	}

	if(nextFig != -1)
		showNext(figures[nextFig], fi);
	showScore(lines, curScore, speed, level, 1.0, 1.0, 1.0);
	if(pause)
		showPause(fi);

	applyCamera(&camera, fig.x, fig.y);

	glTranslatef(camera.x-6.0,camera.y+10.0,0.0);
	drwField();
	if( (parameters.grfBackgrTexture) && (camera.mode != 2) ){
		drwBackgr();
	}
	if( (parameters.grfBottomTexture) && (camera.mode != 1) && (camera.mode != 3) ){
		drwGroung();
	}
	return 0;
}

//
// ������ �������� �����
//
int drwDelLine(int index)
{
	const double rv[20] = 
		{0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 
		1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1};
	const double gv[20] = 
		{1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 
		0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
	const double bv[20] = 
		{0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 0.9, 0.8, 0.7, 0.6, 
		0.5, 0.4, 0.3, 0.2, 0.1, 0.0, 0.1, 0.2, 0.3, 0.4};
	struct _timeb tbeg, tend;
	int ft[field.width][field.height];
	double a = 0.0;
	for(int i=1; i<field.width-1; i++){
		_ftime(&tbeg);
		_ftime(&tend);
		while( ((tend.time-tbeg.time)*1000+tend.millitm-tbeg.millitm) < 50 ){
			_ftime(&tend);
		}
		SwapBuffers(hDC);
		field.ar[i][index] = 22;
		drwGame();
		showScore(lines, curScore, speed, level, rv[i], gv[i], bv[i]);
	}
	if( parameters.sndSoundInGame ){
		PlaySound("sounds\\line.wav", NULL, SND_ASYNC);
	}
	for(int i=1; i<field.width-1; i++){
		_ftime(&tbeg);
		_ftime(&tend);
		while( ((tend.time-tbeg.time)*1000+tend.millitm)-tbeg.millitm < 50 ){
			_ftime(&tend);
		}
		SwapBuffers(hDC);
		field.ar[i][index] = 0;
		drwGame();
		showScore(lines, curScore, speed, level, rv[i+9], gv[i+9], bv[i+9]);
	}
	for(int n=0; n<field.height; n++){
		for(int i=0; i<field.width; i++){
			ft[i][n] = field.ar[i][n];
		}
	}
	for(int n=index; n>=2; n--){
		for(int i=1; i<field.width-1; i++){
			field.ar[i][n] = 0;
		}
	}
	MSG msg;
	int canExit = 0;
	while( !canExit ){
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
			if(msg.message != WM_QUIT){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			_ftime(&tbeg);
			_ftime(&tend);
			while( ((tend.time-tbeg.time)*1000+tend.millitm)-tbeg.millitm < 15 ){
				_ftime(&tend);
			}
			SwapBuffers(hDC);
			drwGame();
			for(int n=index; n>=2; n--){
				for(int i=1; i<field.width-1; i++){
					if(ft[i][n] != 0){
						drwCube(i,n+a,-0.5,ft[i][n]);
					}
				}
			}
			a += 0.1;
			if( a >= 1.0 ){
				a == 0.0;
				canExit = 1;
			}
		}
	}
	if( parameters.sndSoundInGame ){
		PlaySound("sounds\\fall.wav", NULL, SND_ASYNC);
	}
	for(int n=0; n<field.height; n++){
		for(int i=0; i<field.width; i++){
			field.ar[i][n] = ft[i][n];
		}
	}
	return 0;
}

//
// ������ ���
//
int drwCube(float x, float y, float z, int col)
{
	y *= -1;

	switch(col){
		case 0:
			glColor3f(1.0, 1.0, 1.0); break;
		case 1:
			glColor3f(1.0, 0.0, 0.0); break;
		case 2:
			glColor3f(1.0, 0.5, 0.0); break;
		case 3:
			glColor3f(1.0, 1.0, 0.0); break;
		case 4:
			glColor3f(0.0, 0.8, 0.0); break;
		case 5:
			glColor3f(0.0, 0.8, 1.0); break;
		case 6:
			glColor3f(0.0, 0.0, 1.0); break;
		case 7:
			glColor3f(0.6, 0.0, 0.8); break;
		case 8:
			glColor3f(0.7, 1.0, 0.0); break;
		case 9:
			glColor3f(0.75, 0.45, 0.2); break;
		case 10:
			glColor3f(0.8, 0.15, 0.8); break;
		case 11:
			glColor3f(0.0, 0.55, 0.55); break;
		case 12:
			glColor3f(0.2, 0.0, 0.55); break;
		case 20:
			glColor3f(0.3, 0.3, 0.3); break;
		case 21:
			glColor3f(0.5, 0.5, 0.5); break;
		case 22:
			glColor3f(1.0, 1.0, 1.0); break;
	}

	// ������ �����
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(x,y,z);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(x+1,y,z);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(x+1,y,z+1);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(x,y,z+1);
	glEnd();

	// ������ �����
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(x,y,z);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(x+1,y,z);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(x+1,y+1,z);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(x,y+1,z);
	glEnd();

	// ����� �����
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(x,y,z);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(x,y,z+1);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(x,y+1,z+1);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(x,y+1,z);
	glEnd();

	// ������� �����
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(x,y+1,z);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(x+1,y+1,z);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(x+1,y+1,z+1);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(x,y+1,z+1);
	glEnd();

	// �������� �����
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(x,y,z+1);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(x+1,y,z+1);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(x+1,y+1,z+1);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(x,y+1,z+1);
	glEnd();

	// ������ �����
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(x+1,y,z);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(x+1,y,z+1);
	glTexCoord2f(1.0, 1.0);
	glVertex3f(x+1,y+1,z+1);
	glTexCoord2f(0.0, 1.0);
	glVertex3f(x+1,y+1,z);
	glEnd();

	return 0;
}

//
// ����� ������ �� �����
//
//    ������� ���������:
//       x, y                     ����������
//       rd, gr, bl               ���������� ��������, ��������, ������ ��������� ����� ������
//       src                      ������ �������� �������� ������
//
int glPrint(float x, float y, float rd, float gr, float bl, const char *src, ...)
{
	char text[256];
	va_list ap;
	if (src == NULL)
		return 0;

	va_start(ap, src);
	vsprintf(text, src, ap);
	va_end(ap);

	glDisable(GL_TEXTURE_2D);

	glColor3f(rd, gr, bl);
	glRasterPos2f(x,y);

	glPushAttrib(GL_LIST_BIT);
	glListBase(base - 32);
	glCallLists(strlen(text), GL_UNSIGNED_BYTE, text);
	glPopAttrib();

	glEnable(GL_TEXTURE_2D);
	return 0;
}

//
// �������� ������ ��������
//
//    ������� ���������:
//       x, y, z                  ����������
//       angx, angy, angz         ���� �������� ������������ ��������������� ����
//       col                      ����
//       src                      �������� ������
//
int drwWord(float x, float y, float z, float angx, float angy, float angz, int col, char *src)
{
	glTranslatef(x,y,z);
	glRotatef(angx,1.0,0.0,0.0);
	glRotatef(angy,0.0,1.0,0.0);
	glRotatef(angz,0.0,0.0,1.0);
	for(int i=0; i<strlen(src); i++){
		applyTexture(src[i]-63);
		drwCube(-0.5, 0.5, -0.5, col);
		glTranslatef(1.0,0.0,0.0);
	}
	for(int i=0; i<strlen(src); i++){
		glTranslatef(-1.0,0.0,0.0);
	}
	glRotatef(-angx,1.0,0.0,0.0);
	glRotatef(-angy,0.0,1.0,0.0);
	glRotatef(-angz,0.0,0.0,1.0);
	glTranslatef(-x,-y,-z);
	applyTexture(0);
	return 0;
}

//
// ���������� ��������� ������
//
int showNext(int **ar, float fi)
{
	const float x = 5.0, y = 4.5;

	glLoadIdentity();
	glTranslatef(x, y, -15.0f);
	glRotatef(fi,0.0f,1.0f,0.0f);
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
			if(ar[i][j] != 0) drwCube(i-1.5,j,-0.5,ar[i][j]);
	return 0;
}

//
// ����������� "PAUSE"
//
int showPause(float fi)
{
	const float x = -2.0, y = 1.0;

	glLoadIdentity();
	glTranslatef(x, y, -15.0f);

	drwWord(0.0, 0.0, 0.0, fi, 0.0, 0.0, 1, "PAUSE");
	return 0;
}

//
// ����������� "GAMEOVER"
//
int showGameover(float fi)
{
	const float x = -3.5, y = 1.0;

	glLoadIdentity();
	glTranslatef(x, y, -15.0f);

	drwWord(0.0, 0.0, 0.0, fi, 0.0, 0.0, 1, "GAMEOVER");
	return 0;
}

//
// ����������� �����
//
int showScore(int lin, int sc, int spd, int lev, float rd, float gr, float bl)
{
	const float x = -10.0, y = 8.0;
	char st[256], tmpSt[256];
	glLoadIdentity();
	glTranslatef(x, y, -20.0);
	strcpy(st,"score  ");
	strcat(st,_itoa(sc,tmpSt,10));
	glPrint(0.0,-1.0,rd,gr,bl,st);
	strcpy(st,"lines  ");
	strcat(st,_itoa(lin,tmpSt,10));
	glPrint(0.0,-2.0,rd,gr,bl,st);
	strcpy(st,"speed  ");
	strcat(st,_itoa(spd,tmpSt,10));
	glPrint(0.0,-3.0,rd,gr,bl,st);
	strcpy(st,"level  ");
	strcat(st,_itoa(lev,tmpSt,10));
	glPrint(0.0,-4.0,rd,gr,bl,st);
	return 0;
}
