/*
******************************************************************************
*
* File                            highscores.h
*
* Description                     ���������� ������� ��������
*
* Creation date                   02.10.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 MODULE BEGIN
******************************************************************************
*/
#ifndef HIGHSCORES_H
#define HIGHSCORES_H


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
// �����������
#include <windows.h>
#include <stdbool.h>
// ����������������
#include "window.h"
#include "interface.h"


/*
******************************************************************************
*                                 GLOBAL DATA TYPES
******************************************************************************
*/
typedef struct {
   char  name[11];                // ���
   int   score;                   // ����
   int   lines;                   // ���-�� �����
   int   level;                   // �������
} TRecordTableItem;


/*
******************************************************************************
*                                 GLOBAL VARIABLES
******************************************************************************
*/
TRecordTableItem recordTable[10]; // ������� ��������
FILE *fRecordTable;               // ���� ���������� ������� ��������


/*
******************************************************************************
*                                 GLOBAL PROTOTYPES
******************************************************************************
*/
int   readRecords(void);          // ������ ������� �������� �� �����
int   saveRecords(void);          // ������ ������� �������� � ����
bool  chkRecord(int, int, int);   // �������� �� ����� ������


/*
******************************************************************************
*                                 MODULE END
******************************************************************************
*/
#endif
