/*
******************************************************************************
*
* File                            main.c
*
* Description                     ���������� ������� WinMain
*
* Creation date                   20.09.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
// �����������
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <stdbool.h>
#include <sys\timeb.h>
// ��� ������ � OpenGL
#include <gl\gl.h>
#include <gl\glu.h>
#include <gl\glaux.h>
// ����������������
#include "window.h"
#include "init.h"
#include "interface.h"
#include "highscores.h"
#include "camera.h"
#include "options.h"
#include "menu.h"
#include "menuitems.h"
#include "game.h"
#include "gamemenu.h"


/*
******************************************************************************
*                                 MAIN FUNCTION
******************************************************************************
*/

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, 
	LPSTR lpCmdLine, int nCmdShow)
{
	int item = 0;
	int canExit = 0;
	MSG msg;
	bool enterPress = false;

	readOptions();
	if(parameters.grfWndSize == 1) createWindow(1024,768); else createWindow(800,600);

	initTextures();
	initGL();

	initField();
	initFig();
	createFigures(figures,48);
	initFont();
	readRecords();

	drwMenu(item);
	while(!canExit){
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
			if(msg.message != WM_QUIT){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}else{
				canExit = 1;
			}
		}else{
			drwMenu(item);
			SwapBuffers(hDC);
			if(keys[VK_UP]){
				keys[VK_UP] = false;
				item--;
				if(item < 0) item = 5;
				if( parameters.sndSoundInMenu ){
					PlaySound("sounds\\item.wav", NULL, SND_ASYNC);
				}
			}
			if(keys[VK_DOWN]){
				keys[VK_DOWN] = false;
				item++;
				item %= 6;
				if( parameters.sndSoundInMenu ){
					PlaySound("sounds\\item.wav", NULL, SND_ASYNC);
				}
			}
			if( (keys[13]) || (enterPress) ){
				keys[13] = false;
				if( (parameters.sndSoundInMenu) && (!enterPress) ){
					PlaySound("sounds\\select.wav", NULL, SND_SYNC);
				}
				enterPress = false;
				switch(item){
					case 0:
						switch( initGame() ){
							case 1:
								showHighscores();
								break;
							case 2:
								enterPress = true;
								break;
							case 4:
								canExit = 1;
								break;
						}
						break;
					case 1:
						showHighscores();
						break;
					case 2:
						showOptions();
						break;
					case 3:
						showHelp();
						break;
					case 4:
						showAbout();
						break;
					case 5:
						canExit = 1;
						break;
				}
			}
		}
	}
	destroyAr(field.ar);
	destroyAr(fig.ar);
	for(int i=0; i<48; i++) destroyAr(figures[i]);
	destroyFont();
	destroyWindow();
	return (msg.wParam);
}
