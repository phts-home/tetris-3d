/*
******************************************************************************
*
* File                            menu.c
*
* Description                     ���� ���������� ������ menu.h
*
* Creation date                   02.10.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "menu.h"


/*
******************************************************************************
*                                 LOCAL VARIABLES
******************************************************************************
*/
float angc    = 0.0;              // ���� �������� ���� � ����
float angw    = 0.0;              // ���� �������� ����� � ����
int   delay1  = 0;                // ��������
int   delay2  = 0;                // ��������
int   ci      = 0;                // ������ �������� �������


/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
int   drwLogo(void);              // ��������� "TETRIS 3D" � ������� ����
int   drwMenuScr(int);            // ��������� �������� ����


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ��������� ����������� ������ ����
//
//    ������� ���������:
//       item                     ������ ����������� ������
//
int drwMenu(int item)
{
	drwMenuScr(item);
	glLoadIdentity();
	glTranslatef(0.5 + item, 2.0-1.5*item, -20.0);
	glRotatef(angw,1.0,0.0,0.0);
	applyTexture(28);
	drwCube(-0.5, 0.5, -0.5, 0);
	applyTexture(0);
	glRotatef(-angw,1.0,0.0,0.0);
	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ��������� "TETRIS 3D" � ������� ����
//
int drwLogo(void)
{
	// T
	glLoadIdentity();
	glTranslatef(-9.5,7.0,-20.0);
	drwCube(-0.5, 0.5, -0.5, 1);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 1);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 1);
	glTranslatef(-1.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 1);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 1);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 1);
	// E
	glTranslatef(2.0, 3.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	glTranslatef(1.0, 3.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	glTranslatef(-1.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	glTranslatef(-1.0, -2.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 2);
	// T
	glTranslatef(1.0,3.0,0.0);
	drwCube(-0.5, 0.5, -0.5, 3);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 3);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 3);
	glTranslatef(-1.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 3);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 3);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 3);
	// R
	glTranslatef(2.0, 3.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 4);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 4);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 4);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 4);
	glTranslatef(1.0, 3.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 4);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 4);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 4);
	glTranslatef(-1.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 4);
	glTranslatef(1.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 4);
	// I
	glTranslatef(1.0, 3.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 5);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 5);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 5);
	glTranslatef(-1.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 5);
	glTranslatef(0.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 5);
	glTranslatef(-1.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 5);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 5);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 5);
	// S
	glTranslatef(1.0, 3.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 6);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 6);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 6);
	glTranslatef(-2.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 6);
	glTranslatef(2.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 6);
	glTranslatef(-2.0, -1.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 6);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 6);
	glTranslatef(1.0, 0.0, 0.0);
	drwCube(-0.5, 0.5, -0.5, 6);
	glTranslatef(-1.0, 1.5, 0.0);
	drwCube(-0.5, 0.5, -0.5, 6);
	// 3D
	applyTexture(1);
	glLoadIdentity();
	glTranslatef(9.5, 7.0, -20.0);
	angc += 0.5;
	glRotatef(angc,1.0,1.0,1.0);
	drwCube(-0.5, 0.5, -0.5, 7);
	glRotatef(-angc,1.0,1.0,1.0);
	applyTexture(0);
	return 0;
}

//
// ��������� �������� ����
//
int drwMenuScr(int item)
{
	glClearColor(0.0, 0.0, 0.0, 0.5);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	drwLogo();
	glLoadIdentity();

	char w[10];
	char sw[2];

	if(item != 0){
		drwWord(1.5, 2.0, -20.0, 0.0, 0.0, 0.0, 21, "STARTGAME");
	} else {
		strcpy(w,"STARTGAME");
	}

	if(item != 1){
		drwWord(2.5, 0.5, -20.0, 0.0, 0.0, 0.0, 21, "HISCORES");
	} else {
		strcpy(w,"HISCORES");
	}

	if(item != 2){
		drwWord(3.5, -1.0, -20.0, 0.0, 0.0, 0.0, 21, "OPTIONS");
	} else {
		strcpy(w,"OPTIONS");
	}

	if(item != 3){
		drwWord(4.5, -2.5, -20.0, 0.0, 0.0, 0.0, 21, "INFORM");
	} else {
		strcpy(w,"INFORM");
	}

	if(item != 4){
		drwWord(5.5, -4.0, -20.0, 0.0, 0.0, 0.0, 21, "ABOUT");
	} else {
		strcpy(w,"ABOUT");
	}

	if(item != 5){
		drwWord(6.5, -5.5, -20.0, 0.0, 0.0, 0.0, 21, "QUIT");
	} else {
		strcpy(w,"QUIT");
	}

	if(ci >= strlen(w)){
		ci = 0;
		delay1 = 0;
	}
	sw[0] = w[ci];
	sw[1] = 0;
	drwWord(1.5+item*1.0, 2.0-item*1.5, -20.0, angw, 0.0, 0.0, 21, w);
	if(delay1 >= 300){
		drwWord(1.5+item*1.0 + ci*1.0, 2.0-item*1.5, -20.0, angw, 0.0, 0.0, 0, sw);
		if(delay2 == 5){
			delay2 = 0;
			ci++;
			if(ci >= strlen(w)){
				ci = 0;
				delay1 = 0;
			}
		}else{
			delay2++;
		}
	}else{
		delay1++;
	}

	if( parameters.intrfMenuAnimation ){
		angw += 0.2;
	} else {
		angw = 0.0;
	}
	return 0;
}
