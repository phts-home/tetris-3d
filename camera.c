/*
******************************************************************************
*
* File                            camera.c
*
* Description                     ���� ���������� ������ camera.h
*
* Creation date                   02.10.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "camera.h"


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ����������� ������
//
int moveCamera(TCamera *camera, float deltax, float deltay)
{
	camera->x += deltax;
	camera->y += deltay;
	camera->mode = -1;
	return 0;
}

//
// �������� ������
//
int rotateCamera(TCamera *camera, float deltaAngX, float deltaAngY)
{
	camera->angX += deltaAngX;
	camera->angY += deltaAngY;
	camera->mode = -1;
	return 0;
}

//
// ��������� ������ � ����������� � �������� �������
//
int setCamera(TCamera *camera, int mode)
{
	camera->mode = mode;
	switch(mode){
		case 0:
			camera->x = 0.0;
			camera->y = 0.0;
			camera->angX = 0.0;
			camera->angY = 0.0;
			camera->zoom = -30.0;
			break;
		case 1:
			camera->x = 0.0;
			camera->y = 1.0;
			camera->angX = -30.0;
			camera->angY = 10.0;
			camera->zoom = -25.0;
			break;
		case 2:
			camera->x = 0.0;
			camera->y = 0.0;
			camera->angX = 110.0;
			camera->angY = 0.0;
			camera->zoom = -28.0;
			break;
		case 3:
			camera->x = 0.0;
			camera->y = 1.5;
			camera->angX = -45.0;
			camera->angY = 0.0;
			camera->zoom = -20.0;
			break;
	}
	return 0;
}

//
// ����� ����������� �� ������ �� �����
//
int applyCamera(TCamera *camera, int figX, int figY)
{
	if(camera->mode == 2){
		camera->x = -figX*0.5 + 2.5;
		camera->y = figY*0.5;
	}
	glLoadIdentity();
	glTranslatef(0.0, 0.0, camera->zoom);
	
	glRotatef(camera->angX, 1.0, 0.0, 0.0);
	glRotatef(camera->angY, 0.0, 1.0, 0.0);

	glTranslatef(camera->x, camera->y, 0.0);
	return 0;
}
