/*
******************************************************************************
*
* File                            game.c
*
* Description                     ���� ���������� ������ game.h
*
* Creation date                   21.09.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "game.h"


/*
******************************************************************************
*                                 LOCAL VARIABLES
******************************************************************************
*/
int   oldScore;                     // ����

/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
int   clearField(void);           // ������� �������� ����

bool  ifGameOver(void);           // ��������, ��������� �� ����
int   pauseGame(void);            // ������������ ������ �����

int   moveFig(int);               // �������� ������
bool  canMove(int);               // ��������, ����� �� ����������� ������
int   applyMove(void);            // ���������� �������� ������
int   fastMove(void);             // ������� ������� ������
int   rorateFig(void);            // ������� ������
bool  canRotate(void);            // ��������, ����� �� ��������� ������

int   delLine(int);               // �������� �����
int   chkLines(void);             // ��������� �� ������� ������ ����� � ������� ��
int   chkLevel(void);             // �������� �� ������� �� ����� �������


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ������������� ����
//
//    ������������ ��������
//       0                        ������� ���
//       1                        ������ ����
//       2                        ����������
//       3                        "exit to menu"
//       4                        "exit to win"
//
int initGame(void)
{
	MSG msg;
	int canExit = 0;
	srand((unsigned)time(NULL));   // ����. ������������������ ����������
	clearField();
	setCamera(&camera,0);
	fi = 0.0;
	nextFig = -1;
	glClearColor(0.0, 0.0, 0.0, 0.5);
	drwGame();
	pause = false;
	curScore = 0;
	lines = 0;
	speed = 1;
	level = 1;
	_ftime(&fitbeg);

	int res = startGame();

	switch(res){
		case 0:
			if( !chkRecord(curScore, level, lines) ){
				while( !canExit ){
					if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
						if(msg.message != WM_QUIT){
							TranslateMessage(&msg);
							DispatchMessage(&msg);
						}
					}else{
						SwapBuffers(hDC);
						if( (keys[27]) || (keys[13]) || (keys[32]) ){
							keys[27] = false;
							keys[13] = false;
							keys[32] = false;
							canExit = 1;
						}
						drwGame();
						showGameover(fi);
					}
				}
			}else return 1;
			return 0;
			break;
		case 2:
			return 2;
			break;
		case 3:
			return 3;
			break;
		case 4:
			return 4;
			break;
	}
	return 0;
}

//
// ������ ����� ����
//
//    ������������ ��������
//       0                        ����� ����
//       1                        ���� ��������
//       2                        ����������
//       3                        "exit to menu"
//       4                        "exit to win"
//
int startGame(void)
{
	MSG msg;
	struct _timeb tbeg, tend;
	int canExit;
	int l;
	nextFig = rand() % 28;
	_ftime(&tbeg);
	while( !ifGameOver() ){
		curFig = nextFig;
		fig.ar = figures[curFig];
		l = level;
		if(l>6) l = 6;
		nextFig = rand() % (28 + 4*(l-1));
		drwGame();
		fig.x = 4;
		fig.y = 1;
		canExit = 0;
		fi = -60.0;
		while( !canExit ){
			if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
				if(msg.message != WM_QUIT){
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
			}else{
				if(keys[27]){
					keys[27] = false;
					switch( showGamemenu() ){
						case 1:
							return 2;
							break;
						case 3:
							return 3;
							break;
						case 4:
							return 4;
							break;
					}
				}
				if(keys[parameters.ctrlKeys[KEY_GAME_PAUSE]]){
					keys[parameters.ctrlKeys[KEY_GAME_PAUSE]] = false;
					pauseGame();
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_RUP]]){
					keys[parameters.ctrlKeys[KEY_CAM_RUP]] = false;
					rotateCamera(&camera,5.0, 0.0);
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_RDOWN]]){
					keys[parameters.ctrlKeys[KEY_CAM_RDOWN]] = false;
					rotateCamera(&camera,-5.0, 0.0);
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_RLEFT]]){
					keys[parameters.ctrlKeys[KEY_CAM_RLEFT]] = false;
					rotateCamera(&camera,0.0, 5.0);
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_RRIGHT]]){
					keys[parameters.ctrlKeys[KEY_CAM_RRIGHT]] = false;
					rotateCamera(&camera,0.0, -5.0);
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_ZOOMIN]]){
					keys[parameters.ctrlKeys[KEY_CAM_ZOOMIN]] = false;
					camera.zoom++;
					camera.mode = -1;
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_ZOOMOUT]]){
					keys[parameters.ctrlKeys[KEY_CAM_ZOOMOUT]] = false;
					camera.zoom--;
					camera.mode = -1;
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_UP]]){
					keys[parameters.ctrlKeys[KEY_CAM_UP]] = false;
					moveCamera(&camera,0.0, -0.5);
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_DOWN]]){
					keys[parameters.ctrlKeys[KEY_CAM_DOWN]] = false;
					moveCamera(&camera,0.0, 0.5);
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_LEFT]]){
					keys[parameters.ctrlKeys[KEY_CAM_LEFT]] = false;
					moveCamera(&camera,0.5, 0.0);
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_RIGHT]]){
					keys[parameters.ctrlKeys[KEY_CAM_RIGHT]] = false;
					moveCamera(&camera,-0.5, 0.0);
				}
				if(keys[parameters.ctrlKeys[KEY_CAM_SWITCH]]){
					keys[parameters.ctrlKeys[KEY_CAM_SWITCH]] = false;
					camera.mode++;
					camera.mode %= 4;
					setCamera(&camera,camera.mode);
				}
				drwGame();
				drwFig();
				if( !pause ){
					SwapBuffers(hDC);
					if(keys[parameters.ctrlKeys[KEY_FIG_RIGHT]]){
						keys[parameters.ctrlKeys[KEY_FIG_RIGHT]] = false;
						if(canMove(1)){
							moveFig(1);
							if( parameters.sndSoundInGame ){
								PlaySound("sounds\\move.wav", NULL, SND_ASYNC);
							}
						}
					}
					if(keys[parameters.ctrlKeys[KEY_FIG_LEFT]]){
						keys[parameters.ctrlKeys[KEY_FIG_LEFT]] = false;
						if(canMove(-1)){
							moveFig(-1);
							if( parameters.sndSoundInGame ){
								PlaySound("sounds\\move.wav", NULL, SND_ASYNC);
							}
						}
					}
					if(keys[parameters.ctrlKeys[KEY_FIG_ROTATE]]){
						keys[parameters.ctrlKeys[KEY_FIG_ROTATE]] = false;
						keys[16] = false;
						keys[17] = false;
						if(canRotate()){
							rorateFig();
							if( parameters.sndSoundInGame ){
								PlaySound("sounds\\rotate.wav", NULL, SND_ASYNC);
							}
						}
					}
					if(keys[parameters.ctrlKeys[KEY_FIG_DOWN]]){
						keys[parameters.ctrlKeys[KEY_FIG_DOWN]] = false;
						if(canMove(0)){
							moveFig(0);
							if( parameters.sndSoundInGame ){
								PlaySound("sounds\\move.wav", NULL, SND_ASYNC);
							}
						}
					}
					if(keys[parameters.ctrlKeys[KEY_FIG_FASTMOVE]]){
						keys[parameters.ctrlKeys[KEY_FIG_FASTMOVE]] = false;
						fastMove();
						if( parameters.sndSoundInGame ){
							PlaySound("sounds\\place.wav", NULL, SND_ASYNC);
						}
						canExit = 1;
					}
					_ftime(&tend);
					if( ((tend.time-tbeg.time)*1000+tend.millitm)-tbeg.millitm >= 850-60*speed){
						_ftime(&tbeg);
						if(canMove(0))
							moveFig(0);
						else{
							if( parameters.sndSoundInGame ){
								PlaySound("sounds\\place.wav", NULL, SND_ASYNC);
							}
							canExit = 1;
						}
					}
				}else SwapBuffers(hDC);
			}
		}
		applyMove();
		chkLines();
		chkLevel();
		drwGame();
	}
	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ������� �������� ����
//
int clearField(void)
{
	for(int i=1; i<field.width-1; i++)
		for(int j=1; j<field.height-1; j++)
			field.ar[i][j] = 0;
	for(int i=0; i<field.width; i++){
		field.ar[i][0] = 20;
		field.ar[i][field.height-1] = 20;
	}
	for(int j=0; j<field.height; j++){
		field.ar[0][j] = 20;
		field.ar[field.width-1][j] = 20;
	}
	return 0;
}

//
// ��������, ��������� �� ����
//
//    ������������ ��������:
//       true                     ���� ��������
//       false                    ����� ���� ���� ���
//
bool ifGameOver(void)
{
	for(int i=1; i<field.width-1; i++)
		if(field.ar[i][3] != 0) return true;
	return false;
}

//
// ������������ ������ �����
//
int pauseGame(void)
{
	pause = !pause;
	return 0;
}

//
// �������� ������
//
//    ������� ���������:
//       dir                      ����������� [0 - ���� | 1 - ������ | -1 - �����]
//
int moveFig(int dir)
{
	if(!dir)
		fig.y++;
	else
		fig.x += dir;
	return 0;
}

//
// ��������, ����� �� ����������� ������ � �������� �����������
//
//    ������� ���������:
//       dir                      ����������� [0 - ���� | 1 - ������ | -1 - �����]
//
//    ������������ ��������:
//       true                     ��� ��
//       false                    ������ ����������� ������
//
bool canMove(int dir)
{
	bool res = true;
	if(!dir)
		fig.y++;
	else
		fig.x += dir;
	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++)
			if((fig.ar[i][j] != 0)&&(field.ar[i+fig.x][j+fig.y] != 0)){
				res = false;
				break;
			}
		if(!res) break;
	}
	if(!dir)
		fig.y--;
	else
		fig.x -= dir;
	return res;
}

//
// ���������� �������� ������ � ���������� �� �� ������� ����
//
int applyMove(void)
{
	for(int i=0; i<4; i++)
		for(int j=0; j<4; j++)
			if(fig.ar[i][j] != 0) field.ar[i+fig.x][j+fig.y] = fig.ar[i][j];
	return 0;
}

//
// ������� ������� ������
//
int fastMove(void)
{
	while(canMove(0)) fig.y++;
	return 0;
}

//
// ������� ������
//
int rorateFig(void)
{
	int m = curFig - curFig % 4;
	curFig++;
	curFig %= 4;
	curFig += m;
	fig.ar = figures[curFig];
	return 0;
}

//
// ��������, ����� �� ��������� ������
//
//    ������������ ��������:
//       true                     ��� ��
//       false                    ������ ����������� ������
//
bool canRotate(void)
{
	bool res = true;
	int m = curFig - curFig % 4;
	curFig++;
	curFig %= 4;
	curFig += m;
	fig.ar = figures[curFig];
	if(!canMove(0)) res = false;
	curFig--;
	curFig %= 4;
	curFig += m;
	fig.ar = figures[curFig];
	return res;
}

//
// �������� �����
//
int delLine(int index)
{
	if( parameters.intrfLineAnimation ){
		drwDelLine(index);
	}
	for(int n=index; n>=2; n--){
		for(int i=1; i<field.width-1; i++){
			field.ar[i][n] = field.ar[i][n-1];
		}
	}
	lines++;
	return 0;
}

//
// ��������� �� ������� ������ ����� � ������� ��
//
int chkLines(void)
{
	int res, amount = 0;
	for(int j=field.height-2; j>=1; j--){
		res = 1;
		for(int i=1; i<field.width-1; i++)
			if(field.ar[i][j] == 0){
				res = 0;
				break;
			}
		if(res){
			amount++;
			delLine(j);
			j++;
		}
	}
	oldScore = curScore;
	if(amount) curScore += 10*amount + (amount-1)*5;
	return 0;
}

//
// �������� �� ������� �� ����� �������
//
int chkLevel(void)
{
	if(oldScore < curScore){
		if((oldScore / 100) < (curScore / 100)){
			speed++;
		}
		if((oldScore / 1000) < (curScore / 1000)){
			level++;
			speed = 1;
		}
	}
	return 0;
}
