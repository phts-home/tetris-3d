/*
******************************************************************************
*
* File                            window.c
*
* Description                     ���� ���������� ������ window.h
*
* Creation date                   01.10.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "window.h"


/*
******************************************************************************
*                                 LOCAL VARIABLES
******************************************************************************
*/
static  HGLRC hRC = NULL;         // ���������� �������� ����������


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ��������� ���������
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch (msg){
		case WM_CLOSE:
			PostQuitMessage(0);
			return 0;
		case WM_KEYDOWN:
			keys[wParam] = true;
			return 0;
		case WM_KEYUP:
			keys[wParam] = false;
			return 0;
		case WM_SIZE:
			resizeGL(LOWORD(lParam),HIWORD(lParam));
			return 0;
	}
	return DefWindowProc(hWnd,msg,wParam,lParam);
}

//
// �������� ����
//
int createWindow(int width, int height)
{
	const char* title = "Tetris 3D";
	int PixelFormat;
	WNDCLASS wc;
	DWORD dwExStyle;
	DWORD dwStyle;

	int x = GetSystemMetrics(SM_CXSCREEN) / 2 - width / 2;
	int y = GetSystemMetrics(SM_CYSCREEN) / 2 - height / 2;

	hWnd = NULL;

	hInstance = GetModuleHandle(NULL);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(8001));
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "Tetris3D";

	if( !RegisterClass(&wc) ){
		MessageBox(NULL,"Fatal error.\n\nFailed to register the window class",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		exit(0);
	}

	ShowCursor(false);
	dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	dwStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;

	hWnd = CreateWindowEx(dwExStyle, "Tetris3D", title,
				dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
				x, y, width, height,
				NULL, NULL, hInstance, NULL);

	if( !hWnd )
	{
		MessageBox(NULL,"Fatal error.\n\nFailed to create the window",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		destroyWindow();
		exit(0);
	}
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW |
		PFD_SUPPORT_OPENGL |
		PFD_DOUBLEBUFFER,
		PFD_TYPE_RGBA,
		0,
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		16,
		0,
		0,
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};
	pfd.cColorBits = 32;

	hDC = GetDC(hWnd);
	if( !hDC ){
		MessageBox(NULL,"Fatal error.\n\nFailed to create a GL device context",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		destroyWindow();
		exit(0);
	}

	PixelFormat = ChoosePixelFormat(hDC,&pfd);
	if ( !PixelFormat ){
		MessageBox(NULL,"Fatal error.\n\nCan't find a suitable pixelformat",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		destroyWindow();
		exit(0);
	}

	if( !SetPixelFormat(hDC,PixelFormat,&pfd) ){
		MessageBox(NULL,"Fatal error.\n\nCan't set the pixelformat",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		destroyWindow();
		exit(0);
	}

	hRC = wglCreateContext(hDC);
	if ( !hRC ){
		MessageBox(NULL,"Fatal error.\n\nFailed to create a GL rendering context",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		destroyWindow();
		exit(0);
	}

	if( !wglMakeCurrent(hDC,hRC) ){
		MessageBox(NULL,"Fatal error.\n\nFailed to activate the GL rendering context",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		destroyWindow();
		exit(0);
	}

	ShowWindow(hWnd,SW_SHOW);
	SetForegroundWindow(hWnd);
	SetFocus(hWnd);

	return 0;
}

//
// ����������� ����
//
int destroyWindow(void)
{
	ShowCursor(true);
	if( hRC ){
		if(!wglMakeCurrent(NULL,NULL)){
			MessageBox(NULL,"Fatal error.\n\nRelease of DC and RC failed",
				"Tetris 3D",MB_OK|MB_ICONERROR);
			exit(0);
		}

	if( !wglDeleteContext(hRC) ){
			MessageBox(NULL,"Fatal error.\n\nRelease rendering context failed",
				"Tetris 3D",MB_OK|MB_ICONERROR);
			exit(0);
		}
		hRC = NULL;
	}

	if( hDC && !ReleaseDC(hWnd,hDC) ){
		MessageBox(NULL,"Fatal error.\n\nRelease device context failed",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		hDC = NULL;
		exit(0);
	}

	if( hWnd && !DestroyWindow(hWnd) ){
		MessageBox(NULL,"Fatal error.\n\nFailed to destroy the window",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		hWnd = NULL;
		exit(0);
	}

	if( !UnregisterClass("Tetris3D",hInstance) ){
		MessageBox(NULL,"Fatal error.\n\nFailed to unregister the window class",
			"Tetris 3D",MB_OK|MB_ICONERROR);
		hInstance = NULL;
		exit(0);
	}
	return 0;
}
