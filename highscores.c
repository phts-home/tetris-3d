/*
******************************************************************************
*
* File                            highscores.c
*
* Description                     ���� ���������� ������ highscores.h
*
* Creation date                   02.10.2007
*
* Author                          ������ �����
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "highscores.h"


/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
char *getName(void);              // ���� ����� ������������


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ������ ������� �������� �� �����
//
int readRecords(void)
{
	fRecordTable = fopen("hiscores.dat", "rb");
	if(fRecordTable == NULL){
		for(int i=0; i<10; i++){
			strcpy(recordTable[i].name, "unknown");
			recordTable[i].score = 0;
			recordTable[i].lines = 0;
			recordTable[i].level = 0;
		}
		saveRecords();
		return 0;
	}
	fread(&recordTable, sizeof(TRecordTableItem [10]), 1, fRecordTable);
	fclose(fRecordTable);
	return 0;
}

//
// ������ ������� �������� � ����
//
int saveRecords(void)
{
	fRecordTable = fopen("hiscores.dat", "wb");
	fwrite(&recordTable, sizeof(TRecordTableItem [10]), 1, fRecordTable);
	fclose(fRecordTable);
	return 0;
}

//
// �������� �� ����� ������ � ��������� � ������� ��������
//
//    ������������ ��������
//       false                    ������� ���
//       true                     ������ ����
//
bool chkRecord(int sc, int lev, int lin)
{
	int pos = 0;
	char st[11];
	readRecords();
	while((pos < 10)&&(recordTable[pos].score >= sc)) pos++;
	if(pos == 10) return false;
	strcpy(st,getName());
	for(int i=9; i>=pos+1; i--){
		strcpy(recordTable[i].name,recordTable[i-1].name);
		recordTable[i].score = recordTable[i-1].score;
		recordTable[i].level = recordTable[i-1].level;
		recordTable[i].lines = recordTable[i-1].lines;
	}
	strcpy(recordTable[pos].name,st);
	recordTable[pos].score = sc;
	recordTable[pos].level = lev;
	recordTable[pos].lines = lin;
	saveRecords();
	return true;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ���� ����� ������������
//
char *getName(void)
{
	MSG msg;
	int canExit = 0;
	int count = 0;
	char *key;
	char *res;
	key = (char *)malloc(11);
	res = (char *)malloc(11);
	strcpy(key,"");
	strcpy(res,"");

	while(!canExit){
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)){
			if(msg.message != WM_QUIT){
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}else{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();
			glTranslatef(-10.0, 7.0, -20.0);
			glPrint(0.0, 0.0, 1.0, 0.0, 0.0, "please, enter your name");
			glPrint(0.0, -1.0, 0.5, 0.5, 0.5, key);
			SwapBuffers(hDC);
			if(keys[27]){
				keys[27] = false;
				strcpy(res,"noname");
				return res;
			}
			if(keys[13]){
				keys[13] = false;
				if(strcmp(key,"") != 0){
					strcpy(res,key);
					return res;
				}
			}
			if(keys[8]){
				keys[8] = false;
				if(count != 0) count--;
				key[count] = 0;
			}
			if(count < 10){
				for(unsigned char i=48; i<58; i++){
					if(keys[i]){
						keys[i] = false;
						key[count] = (char)i;
						count++;
						break;
					}
				}
				for(unsigned char i=65; i<91; i++){
					if(keys[i]){
						keys[i] = false;
						key[count] = (char)i + 32;
						count++;
						break;
					}
				}
				key[count] = 0;
			}
		}
	}
	return res;
}
